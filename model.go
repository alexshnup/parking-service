package main

type Bucket struct {
	Id       string
	Capacity int
}

type Storage interface {
	Reset(bucket *Bucket)
	ResetIfNotExists(bucket *Bucket)
	TryEnter(bucket *Bucket, carLength int) bool
	Exit(bucket *Bucket, carLength int) bool

	GetFreeLength(bucket *Bucket) int
}
