package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

var (
	storage Storage
	buckets []*Bucket
)

func main() {
	addBucket("1", 50*100)
	addBucket("2", 50*100)
	addBucket("3", 50*100)

	storage = NewRedisStorage("redis:6379")
	for _, bucket := range buckets {
		storage.ResetIfNotExists(bucket)
	}

	r := mux.NewRouter()
	r.HandleFunc("/reset", resetAll).Methods("POST")
	r.HandleFunc("/reset/{id}", reset).Methods("POST")
	r.HandleFunc("/info", info).Methods("GET")
	r.HandleFunc("/enter/{length:[0-9]+}", enter).Methods("POST")
	r.HandleFunc("/exit/{id}/{length:[0-9]+}", exit).Methods("POST")
	http.ListenAndServe(":8080", r)
}

func addBucket(id string, capacity int) {
	bucket := &Bucket{Id: id, Capacity: capacity}
	buckets = append(buckets, bucket)
}

func findBucket(id string) (*Bucket, bool) {
	for _, bucket := range buckets {
		if bucket.Id == id {
			return bucket, true
		}
	}
	return nil, false
}

func resetAll(w http.ResponseWriter, r *http.Request) {
	for _, bucket := range buckets {
		storage.Reset(bucket)
	}
	info(w, r)
}

func reset(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bucketId := vars["id"]
	if bucket, ok := findBucket(bucketId); ok {
		storage.Reset(bucket)
		info(w, r)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func info(w http.ResponseWriter, r *http.Request) {
	result := make(map[string]int)
	for _, bucket := range buckets {
		result[bucket.Id] = storage.GetFreeLength(bucket)
	}
	jsonResult, _ := json.Marshal(result)

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResult)
}

func enter(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	carLength, _ := strconv.Atoi(vars["length"])
	if carLength > 0 {
		for _, bucket := range buckets {
			if storage.TryEnter(bucket, carLength) {
				info(w, r)
				return
			}
		}
	}
	w.WriteHeader(http.StatusNotAcceptable)
}

func exit(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bucketId := vars["id"]
	carLength, _ := strconv.Atoi(vars["length"])
	bucket, ok := findBucket(bucketId)
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	storage.Exit(bucket, carLength)
	info(w, r)
}
