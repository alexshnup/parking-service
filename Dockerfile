FROM scratch

WORKDIR /
COPY ./parking-service /
EXPOSE 8080
ENTRYPOINT ["/parking-service"]