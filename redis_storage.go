package main

import (
	"fmt"
	"gopkg.in/redis.v3"
	"strconv"
	"sync"
)

type RedisStorage struct {
	client *redis.Client
	m      sync.Mutex
}

func NewRedisStorage(address string) *RedisStorage {
	storage := &RedisStorage{}
	storage.client = redis.NewClient(&redis.Options{
		Addr: address,
	})
	return storage
}

func (s *RedisStorage) Reset(bucket *Bucket) {
	s.m.Lock()
	defer s.m.Unlock()

	err := s.client.HSet("Bucket", bucket.Id, strconv.Itoa(bucket.Capacity)).Err()
	if err != nil {
		fmt.Printf("Error: Reset: %v\n", err)
	}
}

func (s *RedisStorage) ResetIfNotExists(bucket *Bucket) {
	_, err := s.client.HSetNX("Bucket", bucket.Id, strconv.Itoa(bucket.Capacity)).Result()
	if err != nil {
		fmt.Printf("Error: ResetIfNotExists: %v\n", err)
	}
}

func (s *RedisStorage) TryEnter(bucket *Bucket, carLength int) bool {
	s.m.Lock()
	defer s.m.Unlock()

	freeLength := s.GetFreeLength(bucket)

	if freeLength < carLength {
		return false
	}

	newFreeLength := freeLength - carLength
	return s.setFreeLength(bucket, newFreeLength)
}

func (s *RedisStorage) Exit(bucket *Bucket, carLength int) bool {
	s.m.Lock()
	defer s.m.Unlock()

	freeLength := s.GetFreeLength(bucket)

	newFreeLength := freeLength + carLength
	if newFreeLength > bucket.Capacity {
		newFreeLength = bucket.Capacity
	}

	return s.setFreeLength(bucket, newFreeLength)
}

func (s *RedisStorage) GetFreeLength(bucket *Bucket) int {
	freeLengthStr, err := s.client.HGet("Bucket", bucket.Id).Result()
	if err != nil {
		if err != redis.Nil {
			fmt.Printf("Error: GetFreeLength: %v\n", err)
		}
		return 0
	}

	freeLength, _ := strconv.Atoi(freeLengthStr)
	return freeLength
}

func (s *RedisStorage) setFreeLength(bucket *Bucket, newFreeLength int) bool {
	err := s.client.HSet("Bucket", bucket.Id, strconv.Itoa(newFreeLength)).Err()
	if err != nil {
		fmt.Printf("Error: SetFreeLength: %v\n", err)
		return false
	}

	return true
}
